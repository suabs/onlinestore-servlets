<%-- 
    Document   : dashboard
    Created on : Jul 3, 2015, 1:04:47 AM
    Author     : subhashrijal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="admin/admincss.css" type="text/css" >
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Dashboard</title>
    </head>
     
    <body>
        <jsp:include page="header.jsp"></jsp:include>
        <jsp:include page="navigation.jsp"></jsp:include>
        <jsp:include page="controller.jsp"></jsp:include>
            
       
        
    </body>
</html>
