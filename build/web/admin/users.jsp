<%-- 
    Document   : users
    Created on : Jul 8, 2015, 3:40:27 PM
    Author     : subhashrijal
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="admincss.css" type="text/css" >
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
       <jsp:include page="header.jsp"></jsp:include>
        <jsp:include page="navigation.jsp"></jsp:include>
        
        <jsp:include page="controller.jsp"></jsp:include>
    
        <table>
    
      <th>id</th>
    <th>firstname</th>
    <th>Lastname</th>
    <th>Username</th>
    <th>Email</th>
    <th>Address</th>
    <th>Postal Add</th>
    <th>Phone</th>
    <th>Country</th>
    
    <th>Status</th>
    <th>Remove user</th>
    <th>Dactivate user</th>
    
    

        <c:forEach var="list" items="${list}">
            
         
            <tr><td>${list.id}</td>
             
             <td>${list.firstname}</td>
             
             <td>${list.lastname}</td>
             <td>${list.username}</td>
             <td>${list.email}</td>
             <td>${list.address}</td>
             <td>${list.areacode}</td>
             <td>${list.phone}</td>
             <td>${list.country}</td>
             
             <td><a href="<%=request.getContextPath()%>/admin/activateuser?id=${list.id}"><font color ="red">${list.activation}</font></a></td>
             <td><a href="<%=request.getContextPath()%>/admin/deleteuser?id=${list.id}"><font color ="red">Remove</font></a></td>
             <td><a href="<%=request.getContextPath()%>/admin/dactivateuser?id=${list.id}"><font color ="red">D-activate</font></a></td>
          
          
            
       </c:forEach>
        </tr>
          
            
       
        
        
        
    </table>
        
    </body>
</html>
