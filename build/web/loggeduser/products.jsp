<%-- 
    Document   : products
    Created on : Jul 9, 2015, 9:31:40 PM
    Author     : subhashrijal
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
       <div id="contents">
                <div id="laptops">Laptops</div>
                
                <font color="red"> ${error}</font>
                <c:forEach var="list" items="${products}" >
                  
               <div id= inside">
                 <div id="distribution">
                 <div id="brandname">${list.getBrand()}</div>
                 <div id="image"> <img src="${list.image}" width="100%" height="100%"></div>
                 <div id ="cost">Euro:${list.cost}</div>
                 <div id="model">Model:${list.model}
                     <div id="brandname">Stocks: ${list.stocks} Left</div>
                 
                 <div id="buy"><a href="<%=request.getContextPath()%>/addtocart?id=${list.id}">Add to cart!!</a></div>
                 
                 </div>
                 </div>
             </div>
              
                
                </c:forEach>
               
         </div>
          
    </body>
</html>
