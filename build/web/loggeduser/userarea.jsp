<%-- 
    Document   : dashboard
    Created on : Jul 3, 2015, 1:04:47 AM
    Author     : subhashrijal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    
        <link rel="stylesheet" href="loggeduser/stylesheet.css" type="text/css" >
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Dashboard</title>
    </head>
     
    <body>
    <jsp:include page="header.jsp"></jsp:include>
    <jsp:include page="navigation.jsp"></jsp:include>
     <jsp:include page="loggerboard.jsp"></jsp:include>       
     <jsp:include page="brandnavigation.jsp"></jsp:include>       
      <jsp:include page="products.jsp"></jsp:include>  
    </body>
    
</html>
<jsp:include page="footer.jsp"></jsp:include>