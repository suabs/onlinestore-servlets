<%-- 
    Document   : mycart
    Created on : Jul 12, 2015, 7:22:06 PM
    Author     : subhashrijal
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="AdminController.PurchaseController"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="./loggeduser/stylesheet.css" type="text/css" >
         <link rel="stylesheet" href="loggeduser/stylesheet1.css" type="text/css" >
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="header.jsp"></jsp:include>
    <jsp:include page="navigation.jsp"></jsp:include>
     <jsp:include page="loggerboard.jsp"></jsp:include>       
     <jsp:include page="brandnavigation.jsp"></jsp:include>       
      

     
     <div id="contents">
                <div id="laptops">My Cart</div>
              
                
   <font color="red"> ${added}   </font>
                <c:forEach var="list" items="${products}" >
                  
                     
                    
              <div id= inside">
                 <div id="distribution">
                     <div id="brandname"> <a href="<%=request.getContextPath()%>/loggeduser/deletecart?id=${list.id}">Delete cart</a></div>
                 <div id="brandname">${list.getBrand()}</div>
                 <div id="image"> <img src="${list.image}" width="100%" height="100%"></div>
                 <div id ="cost">Euro:${list.price}</div>
                 <div id="model">Model:${list.model}
                     <div id="brandname"> ${list.quantity} items left in your cart now!!</div>
                    
                          <div id="buy"><a href="<%=request.getContextPath()%>/loggeduser/buyproduct?id=${list.id}&&productid=${list.productid}"><font size="5px">Buy Now!!</p></font></a>
                 </div>
                 </div>
                 </div>
     
             </div>
              
                
                </c:forEach>
             
               
              
               
         </div>
    
    </body>
</html>

<jsp:include page="footer.jsp"></jsp:include>  