<%-- 
    Document   : index
    Created on : Jul 6, 2015, 7:21:36 PM
    Author     : subhashrijal
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="public/stylesheet.css" type="text/css" >
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Public Dashboard</title>
        
    </head>
     
    <body>
       <jsp:include page="header.jsp"></jsp:include>
       <jsp:include page="navigation.jsp"></jsp:include>
       <jsp:include page="brandnavigation.jsp"></jsp:include>
       
       <div id="contents">
                <div id="laptops">Laptops</div>
                
                
                <c:forEach var="list" items="${products}" >
                  
               <div id= inside">
                 <div id="distribution">
                 <div id="brandname">${list.getBrand()}</div>
                 
                 <div id="image"> <img src="${list.image}" width="100%" height="100%"></div>
                 <div id ="cost">Euro:${list.cost}</div>
                 <div id="model">Model:${list.model}
                     <div id="brandname">Stocks: ${list.stocks} Left</div>
                  
                 
                 <div id="buy"><a href="<%=request.getContextPath()%>/addtocart">Add to cart!!</a></div>
                 
                 </div>
                 </div>
             </div>
              
                
                </c:forEach>
         </div>
       
       <jsp:include page="contactus.jsp"></jsp:include>
       
    </body>
    <jsp:include page="footer.jsp"></jsp:include>
</html>
