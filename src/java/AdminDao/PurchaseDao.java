/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdminDao;

import AdminModel.ProductModel;
import AdminModel.purchaseModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import utilities.Database;

/**
 *
 * @author subhashrijal
 */
public class PurchaseDao {
    
    Database database  = new Database();
    Connection con = database.databaseConnection();
   
    public void addToCart(purchaseModel model){
        String query  ="insert into mycart (productid,image,username,brand,model,price) values (?,?,?,?,?,?)";
        try{
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1,model.getProductid());
            stmt.setString(2, model.getImage());
            stmt.setString(3, model.getUsername());
            stmt.setString(4, model.getBrand());
            stmt.setString(5,model.getModel());
            stmt.setInt(6, model.getPrice());
            stmt.executeUpdate();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    
    
    public purchaseModel getAddedCartbyId(int id ) {
       purchaseModel purchasemodel =null;
       
        
        String query = "select * from mycart where productid =?";
        try{
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1,id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                purchasemodel = new purchaseModel();
                purchasemodel.setId(rs.getInt("id"));
                purchasemodel.setUsername(rs.getString("username"));
                 purchasemodel.setProductid(rs.getInt("productid"));
                  purchasemodel.setQuantity(rs.getInt("quantity"));
                  purchasemodel.setModel(rs.getString("model"));
                  purchasemodel.setImage(rs.getString("image"));
                  purchasemodel.setOrderedpices(rs.getInt("orderedpices"));
                  purchasemodel.setPrice(rs.getInt("price"));
                  purchasemodel.setBrand(rs.getString("brand"));
                 
            
            }
    
}catch(Exception ex){
    ex.printStackTrace();
}
        return purchasemodel;
    }
    
    public void quantityIncrement(purchaseModel model){
       
        String query="update mycart set quantity=? where productid =?";
        try{
        PreparedStatement stmt = con.prepareStatement(query);
        stmt.setInt(1, model.getQuantity());
        Date date = new Date();
        
        stmt.setInt(2, model.getProductid());
        stmt.executeUpdate();
        
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
     public void quantityDecrement(purchaseModel model){
       
        String query="update mycart set quantity=? where id =?";
        try{
        PreparedStatement stmt = con.prepareStatement(query);
        stmt.setInt(1, model.getQuantity());
        stmt.setInt(2, model.getId());
        stmt.executeUpdate();
        
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    
    public ArrayList<purchaseModel> getCartByUsername(String username){ 
         purchaseModel model = null;
        ArrayList<purchaseModel> list = new ArrayList<purchaseModel>();
        String query ="select * from mycart where username =?";
        try{
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                model = new purchaseModel();
                model.setId(rs.getInt("id"));
                model.setBrand(rs.getString("brand"));
                model.setModel(rs.getString("model"));
                model.setPrice(rs.getInt("price"));
                model.setImage(rs.getString("image"));
                model.setProductid(rs.getInt("productid"));
                model.setQuantity(rs.getInt("quantity"));
                list.add(model);
                
            }
            
            
        }catch(Exception ex){
            
        }
        return list;
    }
    
    
    
    public void buyProducts(purchaseModel model){
        String query ="update mycart set orderedpices=? where id=?";
        try{
            PreparedStatement stmt = con.prepareStatement(query);
            
            
            stmt.setInt(1, model.getOrderedpices());
            stmt.setInt(2, model.getId());
            
            stmt.executeUpdate();
            
            }catch(Exception ex){
            ex.printStackTrace();
    }
    
}
    public void deleteCart(int id){
        String query ="delete from mycart where id=?";
        try{
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1, id);
            stmt.executeUpdate();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    public void InsertUserOrders(purchaseModel model){
        String query ="insert into userorder (productid,username)values(?,?)";
        try{
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1, model.getProductid());
            stmt.setString(2, model.getUsername());
           
            stmt.executeUpdate();
        }catch(Exception ex){
            
            ex.printStackTrace();
        }
    }
    
    public void incrementOrders(purchaseModel model){
        String query="update userorder set orderedpices=? where productid=?";
        try{
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1, model.getOrderedpices());
            stmt.setInt(2, model.getProductid());
            stmt.executeUpdate();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        
    }
    
    public ArrayList<purchaseModel> getNotification(){
        ArrayList<purchaseModel> list = new ArrayList<purchaseModel>();
        purchaseModel model = null;
        String query ="select * from userorder";
        try{
            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                model = new purchaseModel();
                model.setId(rs.getInt("id"));
               
                model.setOrderedpices(rs.getInt("orderedpices"));
                model.setUsername(rs.getString("username"));
                model.setProductid(rs.getInt("productid"));
            
                model.setStatus(rs.getString("status"));
                list.add(model);
                
                
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return list;
    }
    
    public void setDeliveryStatus(purchaseModel model){
        
        String query ="update userorder set status=? where id=? ";
        try{
             PreparedStatement stmt = con.prepareStatement(query);
             
             stmt.setString(1, model.getStatus());
             stmt.setInt(2, model.getId());
             stmt.executeUpdate();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    public purchaseModel getUserOrderbyId(int id ) {
       purchaseModel purchasemodel =null;
       
        
        String query = "select * from userorder where productid =?";
        try{
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1,id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                purchasemodel = new purchaseModel();
                purchasemodel.setId(rs.getInt("id"));
                purchasemodel.setUsername(rs.getString("username"));
                 purchasemodel.setProductid(rs.getInt("productid"));
                  purchasemodel.setOrderedpices(rs.getInt("orderedpices"));
                 
            }
    
}catch(Exception ex){
    ex.printStackTrace();
}
        return purchasemodel;
    }
    
    
    
   
     public purchaseModel getBoughtProductsByUser(String username){ 
         purchaseModel model = null;
        //ArrayList<purchaseModel> list = new ArrayList<purchaseModel>();
        String query ="select * from userorder where username =?";
        try{
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                model = new purchaseModel();
                model.setId(rs.getInt("id"));
                model.setProductid(rs.getInt("productid"));
               model.setOrderedpices(rs.getInt("orderedpices"));
                model.setStatus(rs.getString("status"));
                
                
            }
            
            
        }catch(Exception ex){
            
        }
        return model;
    }
   
    
    
    public void deleteCart(){
        String query1 ="DELETE FROM mycart WHERE `timestamp` < (NOW() - INTERVAL 50 MINUTE)";
        try{
            PreparedStatement stmt = con.prepareStatement(query1);
            stmt.executeUpdate();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
   
}