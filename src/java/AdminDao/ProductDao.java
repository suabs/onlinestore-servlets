/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdminDao;

import java.sql.Connection;
import utilities.Database;
import AdminDao.ProductDao;
import AdminModel.ProductModel;
import AdminModel.ProductModel;
import UserModel.ProductsModel;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author subhashrijal
 */
public class ProductDao {
    Database database  = new Database();
    Connection con = database.databaseConnection();
   
    
    
    public ArrayList viewProducts(){
        ArrayList<ProductModel> list = new ArrayList();
        String query = "select * from products";
       
        try{
            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next()){
              ProductModel product = new ProductModel();
               product.setId(rs.getInt("id"));
               
               product.setBrand(rs.getString("brand"));
               product.setModel(rs.getString("model"));
               product.setCost(rs.getInt("price"));
               product.setImage(rs.getString("image"));
               
               product.setStocks(rs.getInt("stocks"));
               list.add(product);
               
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        
        return list;
        
    }
    
    
    
    public void addProducts(ProductModel model){
        ProductModel Products=null;
      String query = "insert into products (brand,image, model, price,stocks) values(?,?,?,?,?)";
        try{
            
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, model.getBrand());
            stmt.setString(2, model.getImage());
            stmt.setString(3, model.getModel());
            stmt.setInt(4, model.getCost());
            stmt.setInt(5, model.getStocks());
            
          stmt.executeUpdate();
          
            
            
        }catch(Exception ex){
            ex.printStackTrace();
        }
        
    }
    
    
    public void editProducts(ProductModel products){
        String query = "update products set brand=?,model=?,price=?,stocks=? where id=? ";
        try{
         
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1,products.getBrand());
            stmt.setString(2,products.getModel());
            stmt.setInt(3, products.getCost());
            stmt.setInt(4, products.getStocks());
            stmt.setInt(5, products.getId());
            stmt.executeUpdate();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        
    }
    
    
    
    public ProductModel getProductsbyId(int id ) {
       ProductModel adminmodel =null;
        ProductModel model = new ProductModel(); 
        String query = "select * from products where id = ?";
        try{
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1,id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                adminmodel = new ProductModel();
                adminmodel.setId(rs.getInt("id"));
                adminmodel.setBrand(rs.getString("brand"));
                 adminmodel.setModel(rs.getString("model"));
                 adminmodel.setImage(rs.getString("image"));
                  adminmodel.setCost(rs.getInt("price"));
                 adminmodel.setStocks(rs.getInt("stocks"));
            
             
            }
    
}catch(Exception ex){
    ex.printStackTrace();
}
        return adminmodel;
    }
    
    
    public void productDelete(int id){
        String query = "delete from products where id = ?";
        try{
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1,id);
            stmt.executeUpdate();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    
    public ArrayList<ProductModel> getInfoByBrand(String id){
        ArrayList<ProductModel> list = new ArrayList<ProductModel>(); 
       
        String query = "select* from products where brand=?";
         try{
              PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1,id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                 ProductModel model = new ProductModel();
                  model.setId(rs.getInt("id"));
                model.setBrand(rs.getString("brand"));
                 model.setModel(rs.getString("model"));
                 model.setImage(rs.getString("image"));
                  model.setCost(rs.getInt("price"));
                 model.setStocks(rs.getInt("stocks"));
                 list.add(model);
            }
            
         }catch(Exception ex){
             ex.printStackTrace();
         }
                 
            return list;     
    }
    
    
    public void  addBrand(ProductsModel model){
        String query ="insert into brands (brandname) values(?)";
        try{
             PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1,model.getBrand());
            stmt.executeUpdate();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    
    public ArrayList<ProductsModel>  getBrands(){

        ArrayList<ProductsModel> list = new ArrayList<ProductsModel>();
        
        String query ="select *  from brands";
        try{
             PreparedStatement stmt = con.prepareStatement(query);
             ResultSet rs = stmt.executeQuery();
             
             while(rs.next()){
                ProductsModel model = new ProductsModel();
                 model.setBrand(rs.getString("brandname"));
                 model.setId(rs.getInt("id"));
                 list.add(model);
             }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return list;
    }
    
    public void deleteBrands(int id){
        String query = "delete from brands where id = ?";
        try{
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1,id);
            stmt.executeUpdate();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    public void editBrands(ProductsModel products){
        String query = "update brands set brand=? where id=? ";
        try{
         
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1,products.getBrand());
            stmt.setInt(2,products.getId());
            
            stmt.executeUpdate();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    public ProductsModel getBrandById(int id){
         ProductsModel model = null;
        
        String query ="select *  from brands where id = ?";
        try{
             PreparedStatement stmt = con.prepareStatement(query);
             stmt.setInt(1, id);
             ResultSet rs = stmt.executeQuery();
             
             while(rs.next()){
                 model = new ProductsModel();
                 model.setBrand(rs.getString("brandname"));
                 model.setId(rs.getInt("id"));
                 
             }
    }catch(Exception ex){
        ex.printStackTrace();
    }
   return model;
}
 
    public ProductsModel getBrandbyBrandName(String brand){
         
        ProductsModel model = null;
        String query ="select *  from `brands` where brandname =?";
        try{
             PreparedStatement stmt = con.prepareStatement(query);
             stmt.setString(1, brand);
             ResultSet rs = stmt.executeQuery();
             
             while(rs.next()){
                 model = new ProductsModel();
                 model.setBrand(rs.getString("brandname"));
                 model.setId(rs.getInt("id"));
                 
             }
    }catch(Exception ex){
        ex.printStackTrace();
    }
   return model;
}
    
  public ArrayList<ProductModel> searchProducts(String search){
      ArrayList<ProductModel> list = new ArrayList<ProductModel>();
      String query = "SELECT * FROM `products` WHERE model=?";
      try{
          PreparedStatement stmt = con.prepareStatement(query);
          stmt.setString(1, search);
          ResultSet rs = stmt.executeQuery();
          
          while(rs.next()){
              ProductModel model = new ProductModel();
               model.setId(rs.getInt("id"));
                model.setBrand(rs.getString("brand"));
                 model.setModel(rs.getString("model"));
                  model.setCost(rs.getInt("price"));
                  model.setImage(rs.getString("image"));
                 model.setStocks(rs.getInt("stocks"));
                 list.add(model);
          }
      }catch(Exception ex){
          ex.printStackTrace();
      }
      
      return list;
  }
    
}
