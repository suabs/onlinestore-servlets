/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdminDao;

import AdminModel.AdminLoginModel;
import UserModel.UserModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import utilities.Database;

/**
 *
 * @author subhashrijal
 */
public class AdminLoginDao {
    
    
    
    Database database  = new Database();
    Connection con = database.databaseConnection();
    
    public AdminLoginModel doLogin(AdminLoginModel model){
        AdminLoginModel login = null;
        String query = "select * from admin_login where username =? and password =?";
        try{
            
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, model.getUsername());
            stmt.setString(2, model.getPassword());
            ResultSet rs = stmt.executeQuery();
            
          
           while(rs.next()){
              login= new AdminLoginModel();
               login.setUsername(rs.getString("username"));
               login.setPassword(rs.getString("password"));
               login.setRole(rs.getString("role"));
               login.setId(rs.getInt("id"));
               login.setRole(rs.getString("role"));
               login.setActivation(rs.getBoolean("activation"));
               
           }
           
           
           String query2 ="select role from admin_login where id = ?";
           con.prepareStatement(query2);
           stmt.setInt(1, model.getId());
           rs = stmt.executeQuery();
           while(rs.next()){
               model.setRole(rs.getString("role"));
           }
           
           /*
         
            String query3 ="select activation from admin_login where id = ?";
           con.prepareStatement(query3);
           stmt.setInt(1, model.getId());
           rs = stmt.executeQuery();
           while(rs.next()){
               model.setActivation(rs.getBoolean("activation"));
           }
           /*/
            
            
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return login;
    }
    
                   
    
    public void changePassword(AdminLoginModel modellogin){
        
        
        String query ="update admin_login set password = ?, activation=? where username =?";
        try{
             PreparedStatement stmt = con.prepareStatement(query);
             stmt.setString(1, modellogin.getNewpassword());
             stmt.setBoolean(2, true);
             stmt.setString(3, modellogin.getUsername());
             stmt.executeUpdate();
             
        }catch(Exception ex){
           ex.printStackTrace();
        }
        
    }
        
    public ArrayList<UserModel> getUsers(){
        
    
    
        ArrayList<UserModel> list = new ArrayList<UserModel>();
        String query ="select * from admin_login";
        try{
            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next()){
                UserModel model = new UserModel();
                model.setId(rs.getInt("id"));
                model.setUsername(rs.getString("username"));
                model.setFirstname(rs.getString("firstname"));
                model.setLastname(rs.getString("lastname"));
                model.setAreacode(rs.getInt("areacode"));
                model.setCity(rs.getString("city"));
                model.setPhone(rs.getInt("phone"));
                model.setCountry(rs.getString("country"));
                model.setAddress(rs.getString("address"));
                model.setEmail(rs.getString("email"));
                model.setActivation(rs.getString("activation"));
                list.add(model);
            }
        
            
        
    }catch(Exception ex){
        ex.printStackTrace();
    }
    
    return list;
}
    
    
    public void doActivateUser(AdminLoginModel model){
        String query ="update admin_login set activation=? where id =?";
        try{
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setBoolean(1, true);
            stmt.setInt(2, model.getId());
            stmt.executeUpdate();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    public void deleteUser(int id){
        String query ="delete from admin_login  where id =?";
        try{
            PreparedStatement stmt = con.prepareStatement(query);
            
            stmt.setInt(1, id);
            stmt.executeUpdate();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    
    public void deActivateUser(AdminLoginModel model){
        String query ="update admin_login set activation=? where id =?";
        try{
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setBoolean(1, false);
            stmt.setInt(2, model.getId());
            stmt.executeUpdate();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    /*
   public static void main(String... args){
        AdminLoginDao login  = new AdminLoginDao();
        AdminLoginModel model = new AdminLoginModel();
        model.setUsername("admin");
        model.setPassword("1fa51f4256e33dc82ad365b64403453c");
        AdminLoginModel model1= login.doLogin(model);
        System.out.println(model1);
        System.out.println(model1.isActivation());
        System.out.println(model1.getRole());
        System.out.println(model1.getUsername());
        
        
    }
    /*/
    
    
    
     public UserModel getUsersById(int id){
        
    
    
       
        String query ="select * from admin_login where id =?";
        UserModel model = null;
        try{
            
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next()){
                 model = new UserModel();
                model.setId(rs.getInt("id"));
                model.setUsername(rs.getString("username"));
                model.setFirstname(rs.getString("firstname"));
                model.setLastname(rs.getString("lastname"));
                model.setAreacode(rs.getInt("areacode"));
                model.setCity(rs.getString("city"));
                model.setPhone(rs.getInt("phone"));
                model.setCountry(rs.getString("country"));
                model.setAddress(rs.getString("address"));
                model.setEmail(rs.getString("email"));
                model.setActivation(rs.getString("activation"));
                
            }
        
            
        
    }catch(Exception ex){
        ex.printStackTrace();
    }
    
    return model;
}
    
     public void editUserInformatin(UserModel model){
         String query ="update admin_login set firstname=?,lastname=?,address=?,phone=?,city=?,email=?,areacode=?, country=? where id=?";
         try{
             PreparedStatement stmt = con.prepareStatement(query);
             stmt.setString(1, model.getFirstname());
             stmt.setString(2, model.getLastname());
             stmt.setString(3,model.getAddress());
             stmt.setInt(4, model.getPhone());
             stmt.setString(5, model.getCity());
             stmt.setString(6, model.getEmail());
             stmt.setInt(7, model.getAreacode());
             
             stmt.setString(8, model.getCountry());
             stmt.setInt(9, model.getId());
             stmt.executeUpdate();
             
             
         }catch(Exception ex){
             ex.printStackTrace();
         }
     }
    
}
    
