/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usercontroller;

import AdminDao.AdminLoginDao;
import AdminDao.ProductDao;
import AdminModel.ProductModel;
import UserDao.UserDao;
import UserDao.UserProductDao;
import UserModel.ProductsModel;
import UserModel.UserModel;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author subhashrijal
 */
@WebServlet(name = "UserController", urlPatterns = {"/usercontroller","/loggeduser/userprofile"})
public class UserController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String uri = request.getServletPath();
        try{
            
            UserProductDao userdao = new UserProductDao();
            ArrayList<ProductsModel> lists = userdao.getBrands();
            request.setAttribute("brands", lists);
            
            if(uri.equals("/loggeduser/userprofile")){
                HttpSession session = request.getSession();
                int id = (int)session.getAttribute("id");
              
                UserDao dao = new UserDao();
                ArrayList<UserModel> list = dao.getUserById(id);
                request.setAttribute("userinfo", list);
               
                request.getRequestDispatcher("/loggeduser/userprofile.jsp").forward(request, response);
            }
                
            
            }catch(Exception ex){
                    out.println(ex);
                   }
}
    
@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
       
        String uri = request.getServletPath();
        try{
            
                
           
            
        }catch(Exception ex){
            out.println(ex);
        }
    }
}
