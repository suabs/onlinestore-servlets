/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdminController;

import AdminDao.AdminLoginDao;
import AdminDao.ProductDao;
import AdminDao.PurchaseDao;
import AdminModel.AdminLoginModel;
import AdminModel.ProductModel;
import AdminModel.purchaseModel;
import UserDao.UserDao;
import UserDao.UserProductDao;
import UserModel.ProductsModel;
import UserModel.UserModel;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author subhashrijal
 */
@WebServlet(name = "PurchaseController", urlPatterns = {"/purchasecontroller","/addtocart","/loggeduser/mycart","/loggeduser/buyproduct","/admin/notification","/admin/statuschange","/loggeduser/deletecart","/loggeduser/myproducts","/loggeduser/mycarts"})
public class PurchaseController extends HttpServlet {

    @Override
    public void init(){
        
        try{
            PurchaseDao dao = new PurchaseDao();
            dao.deleteCart();
            
        }catch(Exception ex){
            ex.printStackTrace();
        }
        
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String uri = request.getServletPath();
        
         //getching datas from brands table in database
            UserProductDao userdao = new UserProductDao();
            ArrayList<ProductsModel> lists = userdao.getBrands();
            request.setAttribute("brands", lists);
        
        try{
           
            
            
            
            
            if(uri.equals("/addtocart")){
                
                
                  
            HttpSession session = request.getSession();
           
            String role = (String) session.getAttribute("role");
           
          
            if(role!=null){
            
                switch(role) {
               
                case"user":
                    
                    
                  int id = Integer.parseInt(request.getParameter("id"));
                    PurchaseDao purchasedao = new PurchaseDao();
                    purchaseModel model = purchasedao.getAddedCartbyId(id);
                    
                     ProductDao productdao = new ProductDao();
                       ProductModel productmodel = productdao.getProductsbyId(id);
                    
                    if(model==null){
                      
                        purchaseModel model1 = new purchaseModel();
                        
                        model1.setUsername((String)session.getAttribute("user"));
                        model1.setBrand(productmodel.getBrand());
                        model1.setModel(productmodel.getModel());
                        model1.setPrice(productmodel.getCost());
                        model1.setImage(productmodel.getImage());
                        model1.setProductid(id);
                        //request.setAttribute("brand", productmodel);
                        //request.getRequestDispatcher("/loggeduser/mycart.jsp").forward(request, response);
                        
                    PurchaseDao dao = new PurchaseDao();
                    dao.addToCart(model1);
                    
                    
                    }else{
                        if(model.getQuantity() < productmodel.getStocks()){
                        
                       model.setQuantity(model.getQuantity()+1) ;
                        PurchaseDao dao = new PurchaseDao();
                        dao.quantityIncrement(model);
                        model.setProductid(id);
                       
                    }else{
                             request.setAttribute("added", "Not added!!you cannot add more items then that in stucks");
                    
                    request.getRequestDispatcher("/loggeduser/mycart").forward(request, response);
                        }
                    }
                     request.setAttribute("added", "1 item added");
                    request.getRequestDispatcher("/loggeduser/mycart").forward(request, response);
                    break;
                    
                default:
                    
                    request.getRequestDispatcher("/public/index1.jsp").forward(request, response);
                    
            }
           
                
            } else{
                request.setAttribute("error", "you Must be member to add product in cart. login/signup now");
                 request.getRequestDispatcher("/public/loginform.jsp").forward(request, response);
            }
            }
            
            
            
            if(uri.equals("/loggeduser/buyproduct")){
                
                HttpSession session = request.getSession();
                String user = (String)session.getAttribute("user");
                int id =Integer.parseInt(request.getParameter("id"));
                int productid=Integer.parseInt(request.getParameter("productid"));
                
                //solution for number of stucks of product in product table.
               
                
                
                //checking session if exists
                if(user!=null){
                    
                    purchaseModel model1 = new purchaseModel();
                    PurchaseDao dao = new PurchaseDao();
                   
                    //retriving datas from table 
                     purchaseModel model  = dao.getAddedCartbyId(productid);
                     
                    //decressing quantities after user buys the item
                       
                     model1.setQuantity(model.getQuantity()-1);
                     model1.setId(id);
                     dao.quantityDecrement(model1);
                     
                     //this leads to adding one item to buying list once user buys. 
                     model1.setOrderedpices(model.getOrderedpices()+1);
                     model1.setId(id);
                    dao.buyProducts(model1);
                    
                    
                    //inserting retrivign datas for new input after buying is confirmed after user buys
                    model1.setUsername(model.getUsername());
                    model1.setProductid(model.getProductid());
                    
                    
                    //this checks if the item is already listed into the table. if yes it just increments ordered pisces into else statement
                    
                    purchaseModel  check = dao.getUserOrderbyId(productid);
                    if(check==null){
                        
                        dao.InsertUserOrders(model1);
                    
                    }else{
                        check.setOrderedpices(check.getOrderedpices()+1);
                        check.setProductid(productid);
                        dao.incrementOrders(check);
                    }
                    
                    if(model.getQuantity()<=1){
                    
                       dao.deleteCart(id);
                       request.setAttribute("added","1 item bought successfully");
                          request.getRequestDispatcher("/loggeduser/mycart").forward(request, response);
                }
                    
                    
                        request.getRequestDispatcher("/loggeduser/mycart").forward(request, response);
                }else{
                    request.getRequestDispatcher("/index.jsp").forward(request, response);
                } 
               
                
            }
            
            
            
            
            
            
            
            
            if(uri.equals("/loggeduser/mycart")){
                HttpSession session = request.getSession();
                
                purchaseModel model = new purchaseModel();
                String username = ((String) session.getAttribute("user"));
                PurchaseDao dao = new PurchaseDao();
                ArrayList<purchaseModel> list = new ArrayList<purchaseModel>();
                    list = dao.getCartByUsername(username);
                request.setAttribute("products", list);
               // request.setAttribute("error", username);
                
                
                request.getRequestDispatcher("/loggeduser/mycart.jsp").forward(request, response);
                    }
            
            
             
            
            /*
            if(uri.equals("/loggeduser/buyproduct")){
                HttpSession session = request.getSession();
                int sessionid = (int)session.getAttribute("id");
                AdminLoginDao dao = new AdminLoginDao();
                UserModel model = dao.getUsersById(sessionid);
                request.setAttribute("list", model);
                request.getRequestDispatcher("/loggeduser/userinfo.jsp").forward(request, response);
            }
             */
          
            if(uri.equals("/admin/notification")){
                PurchaseDao dao = new PurchaseDao();
                ArrayList<purchaseModel> list = dao.getNotification();
                request.setAttribute("products", list);
                request.getRequestDispatcher("/admin/notification.jsp").forward(request, response);
            }
            
            if(uri.equals("/admin/statuschange")){
                PurchaseDao dao = new PurchaseDao();
                String delivered="yes";
                int id =Integer.parseInt(request.getParameter("id"));
                purchaseModel model = new purchaseModel();
                model.setStatus(delivered);
                model.setId(id);
                dao.setDeliveryStatus(model);
                
                request.getRequestDispatcher("/admin/notification").forward(request, response);
            }
            
            if(uri.equals("/loggeduser/deletecart")){
                int id=Integer.parseInt(request.getParameter("id"));
                PurchaseDao dao = new PurchaseDao();
                dao.deleteCart(id);
                
                request.getRequestDispatcher("/loggeduser/mycart").forward(request, response);
            }
            
          
            if(uri.equals("/loggeduser/myproducts")){
                
                 HttpSession session = request.getSession();
                
                
                String username = ((String) session.getAttribute("user"));
                PurchaseDao dao = new PurchaseDao();
                purchaseModel model = dao.getBoughtProductsByUser(username);
                ProductDao productdao = new ProductDao();
                ProductModel products = productdao.getProductsbyId(model.getProductid());
                
                
                    request.setAttribute("list", products);
              // request.setAttribute("error", username);
                
                
                request.getRequestDispatcher("/loggeduser/boughtproducts.jsp").forward(request, response);
            }
            
            
            
            
           
           
            
    }catch(Exception ex){
        out.println(ex);
    }
        

    } 
 
 
 @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
       
        String uri = request.getServletPath();
        
         //getching datas from brands table in database
            UserProductDao userdao = new UserProductDao();
            ArrayList<ProductsModel> lists = userdao.getBrands();
            request.setAttribute("brands", lists);
      try{
           
            
            
            
            
           
          
            
        }catch(Exception ex){
            out.println(ex);
        }
    }
}
