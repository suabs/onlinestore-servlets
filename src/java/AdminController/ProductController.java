/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdminController;


import AdminDao.ProductDao;
import AdminModel.ProductModel;
import UserDao.UserProductDao;
import UserModel.ProductsModel;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 *
 * @author subhashrijal
 */
@MultipartConfig
@WebServlet(name = "ProductController", urlPatterns = {"/viewproducts","/admin/editproducts","/admin/addproducts","/admin/deleteproducts","/viewbrands","/admin/addbrand","/admin/viewallbrand","/admin/editbrands","/admin/deletebrands","/search",})
public class ProductController extends HttpServlet {
    
  
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String uri = request.getServletPath();
        try{
            
            //brand made prublic for all methods;
            
            UserProductDao userdao = new UserProductDao();
            ArrayList<ProductsModel> lists = userdao.getBrands();
            request.setAttribute("brands", lists);
          // request.getRequestDispatcher("/public/brandnavigation.jsp").forward(request, response);
           
            if(uri.equals("/viewproducts")){
           
            ProductDao product = new ProductDao();
            ArrayList<ProductModel> list = product.viewProducts();
            request.setAttribute("products", list);
            
            HttpSession session = request.getSession();
           
            String role = (String) session.getAttribute("role");
         
            if(role!=null){
            
                switch(role) {
                case"admin":
                    request.getRequestDispatcher("/admin/products.jsp").forward(request, response);
                    break;
                
                case"user":
                    
                    //request.setAttribute("product", list);
                    request.getRequestDispatcher("/loggeduser/userarea.jsp").forward(request, response);
                    break;
                    
                default:
                    
                    request.getRequestDispatcher("/public/index1.jsp").forward(request, response);
                    
            }
           
                
            } else{
                 request.getRequestDispatcher("/public/index1.jsp").forward(request, response);
            }
            }
            
            
            if(uri.equals("/admin/addproducts")){
                //out.println("hello world");
                request.getRequestDispatcher("/admin/addproducts.jsp").forward(request, response);
                //   response.sendRedirect("/admin/addproducts.jsp");
                }
            
           if(uri.equals("/admin/editproducts")){
              
                ProductDao dao = new ProductDao();
                 int id =Integer.parseInt(request.getParameter("id"));
                 ProductModel model = dao.getProductsbyId(id);
                 out.println(model);
                
                request.setAttribute("edit", model);
                
              request.getRequestDispatcher("/admin/edit.jsp").forward(request, response);
                
            }
           
           if(uri.equals("/admin/deleteproducts")){
                
                int id =Integer.parseInt(request.getParameter("id"));
                ProductModel model = new ProductModel();
                //model.setId(id);
                ProductDao dao = new ProductDao();
                dao.productDelete(id);
                request.getRequestDispatcher("/viewproducts").forward(request, response);
            }
           
           
           if(uri.equals("/viewbrands")){
               
               String model = request.getParameter("id");
               
               ProductDao dao = new ProductDao();
               ArrayList<ProductModel> brand = dao.getInfoByBrand(model);
               request.setAttribute("products", brand);
               HttpSession session = request.getSession();
               
           
            String role = (String) session.getAttribute("role");
           
          
            if(role!=null){
            
                switch(role) {
                case"admin":
                    request.getRequestDispatcher("/admin/viewbrand.jsp").forward(request, response);
                    break;
                
                case"user":
                    
                    //request.setAttribute("product", list);
                    request.getRequestDispatcher("/loggeduser/userarea.jsp").forward(request, response);
                    break;
                    
                default:
                    
                    request.getRequestDispatcher("/public/index1.jsp").forward(request, response);
                    
            }
           
                
            } else{
                 request.getRequestDispatcher("/public/index1.jsp").forward(request, response);
            }
           }   
           
        }catch(Exception ex){
            out.println(ex);
        }
        
        
        
        try{
        //brand part starts
        if(uri.equals("/admin/addbrand")){
            request.getRequestDispatcher("/admin/addbrand.jsp").forward(request, response);
        }
     
        
        
       if(uri.equals("/admin/viewallbrand")){
        ProductDao dao = new ProductDao();
        ArrayList<ProductsModel> list = dao.getBrands();
        request.setAttribute("products", list);
        request.getRequestDispatcher("/admin/viewbrand.jsp").forward(request, response);
       }
       
       
       if(uri.equals("/admin/deletebrands")){
           int id = Integer.parseInt(request.getParameter("id"));
           ProductDao dao = new ProductDao();
           dao.deleteBrands(id);
            request.getRequestDispatcher("/admin/viewallbrand").forward(request, response);
       }
       
       if(uri.equals("/admin/editbrands")){
           int id = Integer.parseInt(request.getParameter("id"));
           ProductDao dao = new ProductDao();
          ProductsModel model = dao.getBrandById(id);
           request.setAttribute("edit", model);
           request.getRequestDispatcher("/admin/editbrands.jsp").forward(request, response);
       }
        }catch(Exception ex){
            out.println(ex);
        }
    }
        

    
 
 
 @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
       
        String uri = request.getServletPath();
        
        
        UserProductDao userdao = new UserProductDao();
            ArrayList<ProductsModel> lists = userdao.getBrands();
            request.setAttribute("brands", lists);
        try{
            
            
           
            if(uri.equals("/admin/addproducts")){
               
                
                
               ProductsModel model = new ProductsModel();
               String brand = request.getParameter("brand");
                 ProductDao productsdao = new ProductDao();
               ProductsModel models = productsdao.getBrandbyBrandName(brand);
              
               if(models!=null){
                


// inserting image.   
   
                   
                   
                   Part fileContent = request.getPart("picture");
    
    //for making unique filename
                   ImagePathAllocator pathallocator = new ImagePathAllocator();
    UUID uuid = UUID.randomUUID();
    String randomUUIDString = uuid.toString();
            String fileName = randomUUIDString+pathallocator.getFilename(fileContent);
            //post.setPictureName(fileName);
             //post.setPicture(fileContent.getInputStream());
            File picture = new File(request.
                    getServletContext()
                    .getRealPath("/Images/pictures")
                    ,fileName);
            
            
                Files.copy(fileContent.getInputStream(), picture.toPath());
    

                
                
                
                
                
                 ProductModel addproducts = new ProductModel();
                addproducts.setBrand(request.getParameter("brand"));
               
                
                
                addproducts.setModel(request.getParameter("model"));
                addproducts.setCost(Integer.parseInt(request.getParameter("cost")));
                addproducts.setStocks(Integer.parseInt(request.getParameter("stocks")));
                
                addproducts.setImage("Images/pictures/"+fileName);
                
               
               
                     productsdao.addProducts(addproducts);
                      request.getRequestDispatcher("/viewproducts").forward(request, response);
               }else{
                   request.setAttribute("error", "this brand is new to us, either check spelling or add it to `brand` table !!!");
                   request.getRequestDispatcher("/admin/addproducts.jsp").forward(request, response);
               }
                 
                
                     
            }
            
                   
            if(uri.equals("/admin/editproducts")){
                ProductModel products = new ProductModel();
                products.setId(Integer.parseInt(request.getParameter("id")));
                products.setBrand(request.getParameter("brand"));
                products.setModel(request.getParameter("model"));
                products.setCost(Integer.parseInt(request.getParameter("cost")));
                products.setStocks(Integer.parseInt(request.getParameter("stocks")));
                
                ProductDao productdao = new ProductDao();
                productdao.editProducts(products);
                
               request.getRequestDispatcher("/viewproducts").forward(request, response);
                
           
            }else if(uri.equals("/viewproducts")){
           
                
                ProductDao product = new ProductDao();
              ArrayList<ProductModel> list = product.viewProducts();
              request.setAttribute("products", list);
                HttpSession session = request.getSession();
                String role = (String) session.getAttribute("role");
            
                //request.setAttribute("product", role);
                //request.getRequestDispatcher("/loggeduser/test.jsp").forward(request, response);
                if(role!=null){
            
                switch(role) {
                case"admin":
                    request.setAttribute("error", " products added successfully");
                    request.getRequestDispatcher("/admin/products.jsp").forward(request, response);
                    break;
                
                case"user":
                    request.setAttribute("products", list);
                    request.getRequestDispatcher("/loggeduser/userarea.jsp").forward(request, response);
                    break;
                    
                default:
                    
                    request.getRequestDispatcher("/public/index1.jsp").forward(request, response);
                    
            }
            } else{
                 request.getRequestDispatcher("/public/index1.jsp").forward(request, response);
            }
            }
            
            if(uri.equals("/admin/addbrand")){
                ProductsModel model = new ProductsModel();
                model.setBrand(request.getParameter("brand"));
                ProductDao dao = new ProductDao();
                dao.addBrand(model);
                
                request.getRequestDispatcher("/admin/viewallbrand").forward(request, response);
                
            }
            
            
            if(uri.equals("/admin/editbrands")){
                ProductsModel model = new ProductsModel();
                model.setBrand(request.getParameter("brand"));
                int id =Integer.parseInt(request.getParameter("id"));
                ProductDao dao = new ProductDao();
                dao.editBrands(model);
                request.getRequestDispatcher("/admin/viewallbrand").forward(request, response);
                
            }else if(uri.equals("/admin/viewallbrand")){
        ProductDao dao = new ProductDao();
        ArrayList<ProductsModel> list = dao.getBrands();
        request.setAttribute("products", list);
        request.getRequestDispatcher("/admin/viewbrand.jsp").forward(request, response);
       }
       
            
            if(uri.equals("/search")){
               String search  = request.getParameter("search");
                 ProductDao dao = new ProductDao();
               ArrayList<ProductModel> list = dao.searchProducts(search);
               
               if(list.equals(null)){
                   request.setAttribute("error", "no items found");
               }else{
                   request.setAttribute("products", list);
               }
               
               HttpSession session = request.getSession();
               
           
            String role = (String) session.getAttribute("role");
           
          
            if(role!=null){
            
                switch(role) {
                case"admin":
                    request.getRequestDispatcher("/admin/products.jsp").forward(request, response);
                    break;
                
                case"user":
                    
                    //request.setAttribute("product", list);
                    request.getRequestDispatcher("/loggeduser/userarea.jsp").forward(request, response);
                    break;
                    
                default:
                    
                    request.getRequestDispatcher("/public/index1.jsp").forward(request, response);
                    
            }
           
                
            } else{
                 request.getRequestDispatcher("/public/index1.jsp").forward(request, response);
            }
               
                
                
                
            }
            
        }catch(Exception ex){
            out.println(ex);
        }
    }
}

    


