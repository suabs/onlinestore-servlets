/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdminController;

import AdminDao.ProductDao;
import AdminDao.AdminLoginDao;
import AdminDao.Md5Encryption;
import AdminModel.ProductModel;
import AdminModel.AdminLoginModel;
import AdminModel.ProductModel;
import UserDao.UserDao;
import UserDao.UserProductDao;
import UserModel.ProductsModel;
import UserModel.UserModel;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author subhashrijal
 */
@WebServlet(name = "AdminController", urlPatterns = {"/login","/logout","/admin/changepassword","/loggeduser/changepassword","/admin/getusers","/admin/activateuser","/admin/dactivateuser","/admin/deleteuser","/signup","/loggeduser/updateinfo"})
public class AdminController extends HttpServlet {

    
 protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String uri = request.getServletPath();
        //for brandnavigation its made public
         UserProductDao userdao = new UserProductDao();
            ArrayList<ProductsModel> lists = userdao.getBrands();
            request.setAttribute("brands", lists);
        try{
            
            if(uri.equals("/admin/getusers")){
                
                AdminLoginDao dao = new AdminLoginDao();
                
                request.setAttribute("list",  dao.getUsers());
                
                request.getRequestDispatcher("/admin/users.jsp").forward(request, response);
            
                       
            }
            
           if(uri.equals("/login")){
               request.getRequestDispatcher("/public/loginform.jsp").forward(request, response);
           }
           
           
          if(uri.equals("/signup")){
                request.getRequestDispatcher("/public/signup.jsp").forward(request, response);
            }
           
           
           if(uri.equals("/logout")){
                HttpSession session = request.getSession(true);
                session.invalidate();
                //String contextpath = request.getContextPath();
                request.getRequestDispatcher("/index.jsp").forward(request, response);
            }
           
           if(uri.equals("/loggeduser/updateinfo")){
               
               HttpSession session = request.getSession();
                int sessionid = (int)session.getAttribute("id");
                AdminLoginDao dao = new AdminLoginDao();
                UserModel model = dao.getUsersById(sessionid);
                request.setAttribute("list", model);
                request.getRequestDispatcher("/loggeduser/userinfo.jsp").forward(request, response);
           }
           
           
           //password change
           
           if(uri.equals("/admin/changepassword") || uri.equals("/loggeduser/changepassword")){
               HttpSession session = request.getSession();
               String role = (String)session.getAttribute("role");
               int id = (int) session.getAttribute("id");
               
               
               if(role!=null){
                  switch(role){
                      case"admin":
                          request.getRequestDispatcher("/admin/changepassword.jsp").forward(request, response);
                          break;
                      case"user":
                          request.getRequestDispatcher("/loggeduser/passwordchange.jsp").forward(request, response);
                          break;
                  
                  }
                  
               }else{
                   request.getRequestDispatcher("/login").forward(request, response);
               }
           }
           //activate user
           
           if(uri.equals("/admin/activateuser")){
               int id  = Integer.parseInt(request.getParameter("id"));
               AdminLoginModel model = new AdminLoginModel();
               model.setActivation(true);
               model.setId(id);
               AdminLoginDao dao = new AdminLoginDao();
               dao.doActivateUser(model);
               request.getRequestDispatcher("/admin/getusers").forward(request, response);
               
           }
           
           if(uri.equals("/admin/deleteuser")){
               int id  = Integer.parseInt(request.getParameter("id"));
               AdminLoginDao dao = new AdminLoginDao();
               dao.deleteUser(id);
               request.getRequestDispatcher("/admin/getusers").forward(request, response);
               
           }
           
            if(uri.equals("/admin/dactivateuser")){
               
               int id  = Integer.parseInt(request.getParameter("id"));
               AdminLoginModel model = new AdminLoginModel();
               model.setActivation(false);
               model.setId(id);
               AdminLoginDao dao = new AdminLoginDao();
               dao.deActivateUser(model);
               request.getRequestDispatcher("/admin/getusers").forward(request, response);
               
           
               
           }
            
            
         
        }catch(Exception ex){
            out.println(ex);
        }
     
       
 }
 
 
 @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
       
        String uri = request.getServletPath();
        
        UserProductDao userdao = new UserProductDao();
            ArrayList<ProductsModel> lists = userdao.getBrands();
            request.setAttribute("brands", lists);
        
        try{
            
            //user Login
            if(uri.equals("/login")){
                
                AdminLoginModel model = new AdminLoginModel();
                model.setUsername(request.getParameter("username"));
                Md5Encryption encryption  = new Md5Encryption();
                String password = request.getParameter("password");
                model.setPassword(encryption.md5(password));
                 
                 
                 AdminLoginDao dao = new AdminLoginDao();
                 AdminLoginModel adminmodel = dao.doLogin(model);
                 
                 if(adminmodel != null){
                     
                     //checking if admin has activated the account
                     if(adminmodel.isActivation()){
                         
                     
             HttpSession session = request.getSession(true);
             //session.setAttribute("sessions", session.getId());
             
             session.setAttribute("role", adminmodel.getRole());
             session.setAttribute("user", adminmodel.getUsername());
             session.setAttribute("id", adminmodel.getId());
             session.setMaxInactiveInterval(2*60*60);
             //Cookie cookies  = new Cookie("session",session.getId());
            
             //response.addCookie(cookies);
                     
             //checking the role
              if(adminmodel.getRole().equals("admin")){
                  request.getRequestDispatcher("/admin/dashboard.jsp").forward(request, response);
              }else{
                  
                      session.setAttribute("role", model.getRole());
                   request.getRequestDispatcher("/viewproducts").forward(request, response);
              }
                     
              
                     } else{
                         request.setAttribute("error", "your account is not yet activated contact Admin!!!");
                         
                     request.getRequestDispatcher("/public/loginform.jsp").forward(request, response);
                         
                     }  
                }else{
                     request.setAttribute("error", "invalid Login attempt!!!");
                     request.setAttribute("error1", "invalid Login attempt!!!");
                     request.getRequestDispatcher("/public/loginform.jsp").forward(request, response);
                 }
            }
            
            //change password 
            
            
            if(uri.equals("/loggeduser/changepassword") || uri.equals("/admin/changepassword") ){
               HttpSession session = request.getSession();
               String user = (String) session.getAttribute("role");
                
               if(user !=null){
                    
                AdminLoginModel model = new AdminLoginModel();
                model.setUsername((String)session.getAttribute("user"));
                //encryption 
                Md5Encryption encryption  = new Md5Encryption();
                String oldpassword = request.getParameter("oldpassword");
                model.setPassword(encryption.md5(oldpassword));
                
               
               //model.setId(Integer.parseInt(request.getParameter("id")));
                AdminLoginDao dao = new AdminLoginDao();
                AdminLoginModel admnimodel = dao.doLogin(model);
               
               
                
                if(admnimodel!=null){
                    String newpassword=request.getParameter("newpassword");
                    String repassword = request.getParameter("repassword");
                    
                   
                
               
                    
                    if(newpassword.equals(repassword)){
                        
                        model.setUsername((String)session.getAttribute("user"));
                        
                        model.setNewpassword(encryption.md5(newpassword));
                        
                        AdminLoginDao changepassword = new AdminLoginDao();
                        changepassword.changePassword(model);
                        //request.setAttribute("error",model.getUsername());
                       request.setAttribute("error","password changed successfully");
                        if(user !=null) {
                        switch((String)session.getAttribute("role")){
                            case"admin":
                                request.getRequestDispatcher("/admin/changepassword.jsp").forward(request, response);
                                break;
                            case"user":
                               
                                    request.getRequestDispatcher("/loggeduser/passwordchange.jsp").forward(request, response);
                                    break;
                            default:
                                request.getRequestDispatcher("/index.jsp").forward(request, response);
                        }
                        }else{
                            request.getRequestDispatcher("/index.jsp").forward(request, response);
                        }
                       
                        
                    }else{
                        request.setAttribute("error", "new passwords do not match");
                        
                        
                        
                        switch((String)session.getAttribute("role")){
                            case"admin":
                                request.getRequestDispatcher("/admin/changepassword.jsp").forward(request, response);
                                break;
                            case"user":
                                    request.getRequestDispatcher("/loggeduser/passwordchange.jsp").forward(request, response);
                                    break;
                            default:
                                request.getRequestDispatcher("/public/loginform.jsp").forward(request, response);
                        }
                    
                    }
                }else{
                    request.setAttribute("error", "old password didnt match");
                    
                    
                     switch((String)session.getAttribute("role")){
                            case"admin":
                                request.getRequestDispatcher("/admin/changepassword.jsp").forward(request, response);
                                break;
                            case"user":
                                    request.getRequestDispatcher("/loggeduser/passwordchange.jsp").forward(request, response);
                                    break;
                            default:
                                request.getRequestDispatcher("/public/loginform.jsp").forward(request, response);
                        }
                    
                }
                
                }else{
                    
                    
                          
                                request.getRequestDispatcher("/public/loginform.jsp").forward(request, response);
                        
                    
                }
                
                
            }
            
                
            if(uri.equals("/signup")){
                    
                    
                    UserModel model = new UserModel();
                    
                    model.setUsername(request.getParameter("username"));
                    UserDao dao = new UserDao();
                    UserModel usermodel = dao.checkUser(model);
                    //checking if the username already exists.
                    
                    if(usermodel==null){
                        
                        String password = request.getParameter("password");
                        
                     String repassword = request.getParameter("repassword");
                    
                     //checking if the password and re password matches.
                   if(password.equals(repassword)){
                     Md5Encryption encryption  = new Md5Encryption();
                
                       model.setFirstname(request.getParameter("firstname"));
                       model.setLastname(request.getParameter("lastname"));
                       model.setAddress(request.getParameter("address"));
                       model.setPhone(Integer.parseInt(request.getParameter("phone")));
                       model.setAreacode(Integer.parseInt(request.getParameter("areacode")));
                       model.setCountry(request.getParameter("country"));
                       model.setCity(request.getParameter("city"));
                       model.setPassword(encryption.md5(password));
                       model.setEmail(request.getParameter("email"));
                    
                    dao.doSignup(model);
                    HttpSession session = request.getSession(true);
                    request.setAttribute("error", "you have signed up successfuly!! login now");
                    request.getRequestDispatcher("/public/loginform.jsp").forward(request, response);
                       
                   }else{
                       request.setAttribute("password", "password/repassword didnt match");
                       request.getRequestDispatcher("/public/signup.jsp").forward(request, response);
                   }
                    
                    }else{
                        request.setAttribute("password", "Username already exists");
                       request.getRequestDispatcher("/public/signup.jsp").forward(request, response);
                    }
                    
                    
                    
                    
                }
            
            
            if(uri.equals("/loggeduser/updateinfo")){
                
                UserModel model = new UserModel();
               model.setId(Integer.parseInt(request.getParameter("id")));
                model.setFirstname(request.getParameter("firstname"));
                model.setLastname(request.getParameter("lastname"));
                model.setAddress(request.getParameter("address"));
               model.setAreacode(Integer.parseInt(request.getParameter("areacode")));
                model.setCity(request.getParameter("city"));
               model.setPhone(Integer.parseInt(request.getParameter("phonenumber")));
                model.setCountry(request.getParameter("country"));
                model.setEmail(request.getParameter("email"));
                
                //request.setAttribute("products", model);
                //request.getRequestDispatcher("/loggeduser/test.jsp").forward(request, response);
                AdminLoginDao dao = new AdminLoginDao();
                dao.editUserInformatin(model);
                request.setAttribute("error", "information changed successfully");
                request.getRequestDispatcher("/loggeduser/userinfo.jsp").forward(request, response);
                
                
            }
            
            
        }catch(Exception ex){
            out.println(ex);
        }
    }
}
