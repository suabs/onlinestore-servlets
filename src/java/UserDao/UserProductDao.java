/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserDao;

import UserModel.ProductsModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import utilities.Database;

/**
 *
 * @author subhashrijal
 */
public class UserProductDao {
    Database database  = new Database();
    Connection con = database.databaseConnection();
   
    
    
    public ArrayList getBrands(){
        ArrayList<ProductsModel> list = new ArrayList();
        String query = "select * from brands";
       
        try{
            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next()){
              ProductsModel product = new ProductsModel();
               product.setId(rs.getInt("id"));
               
               
               product.setBrand(rs.getString("brandname"));
               list.add(product);
               
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        
        return list;
}
    
    
}
