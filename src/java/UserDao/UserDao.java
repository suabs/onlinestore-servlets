/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserDao;

import AdminModel.ProductModel;
import UserModel.UserModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import utilities.Database;

/**
 *
 * @author subhashrijal
 */
public class UserDao {
    
    Database database  = new Database();
    Connection con = database.databaseConnection();
    
    //signup for users
    
    public void doSignup(UserModel model){
        String query ="insert into admin_login (username, password, firstname,lastname,address,phone,city,email,areacode,country) values(?,?,?,?,?,?,?,?,?,?)";
        try{
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, model.getUsername());
            stmt.setString(2, model.getPassword());
            stmt.setString(3, model.getFirstname());
            stmt.setString(4, model.getLastname());
            stmt.setString(5,model.getAddress());
            stmt.setInt(6, model.getPhone());
            stmt.setString(7, model.getCity());
            stmt.setString(8, model.getEmail());
            stmt.setInt(9, model.getAreacode());
            stmt.setString(10, model.getCountry());
            stmt.executeUpdate();
            
        }catch(Exception ex){
            ex.printStackTrace();
        }
        
    }
    
    public UserModel checkUser(UserModel model){
        String query = "select * from admin_login where username = ?";
        UserModel login = null;
       
        try{
            PreparedStatement stmt = con.prepareCall(query);
            stmt.setString(1, model.getUsername());
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                login = new UserModel();
                login.setUsername(rs.getString("username"));
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return login;
    }
    
    public ArrayList<UserModel> getUserById(int id){
        ArrayList<UserModel> list = new ArrayList<UserModel>();
        String query = "select * from admin_login where id = ?";
        try{
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                UserModel model = new UserModel();
                model.setId(rs.getInt("id"));
                model.setUsername(rs.getString("username"));
                model.setFirstname(rs.getString("firstname"));
                model.setLastname(rs.getString("lastname"));
                model.setAreacode(rs.getInt("areacode"));
                model.setCity(rs.getString("city"));
                model.setPhone(rs.getInt("phone"));
                model.setAddress(rs.getString("address"));
                model.setCountry(rs.getString("country"));
                model.setEmail(rs.getString("email"));
                model.setActivation(rs.getString("activation"));
                list.add(model);
            }
            
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return list;
    }
}
