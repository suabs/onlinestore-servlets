<%-- 
    Document   : addproducts
    Created on : Jul 3, 2015, 5:15:41 PM
    Author     : subhashrijal
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="admincss.css" type="text/css" >
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
       <jsp:include page="dashboard.jsp"></jsp:include>
    
       <table>
           
       <form action ="<%=request.getContextPath()%>/admin/editproducts" method ="post" >
           <input type="hidden"  name="id" value =${edit.id} > 
           
           <tr><td>Brand</td>
               <td><input type="text" name="brand" value =${edit.brand} ></td></tr>
           
           <tr><td>Model</td><td>
                   <input type="text" name="model" value=${edit.model} ></td></tr>
           
           <tr><td>Cost</td>
               <td><input type="text" name="cost" value =${edit.cost} ></td></tr>
           
           <tr><td>Stocks</td>
               <td><input type="text" name="stocks" value=${edit.stocks} ></td></tr>
           
           
       
           <tr><td><input type="submit" value="Edit-product"></td></tr>
        </form>
       </table>
    </body>
</html>
