<%-- 
    Document   : products
    Created on : Jul 3, 2015, 4:59:04 PM
    Author     : subhashrijal
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
            
            <link rel="stylesheet" href="stylesheet.css" type="text/css" >
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="dashboard.jsp"></jsp:include>
    
        <table>
          
    <th>id </th>
    <th>brand</th>
    <th>image</th>
    <th>Model</th>
    <th>Cost</th>
    <th>Stocks</th>
    <th>Edit</th>
    <th>Delete</th>
    
    <font color = "red">${error}</font>
        <c:forEach var="list" items="${products}">
            
             <tr><td>${list.getId()}</td>
             
             <td>${list.getBrand()}</td>
             <td><img src="${list.image}" width="90px" height="90px"></td>
             <td>${list.getModel()}</td>
             <td>${list.cost}</td>
             <td>${list.stocks}</td>
          
             <td><a href="<%=request.getContextPath()%>/admin/editproducts?id=${list.id}"><font color ="red">Edit</font></a></td>
             <td><a href="<%=request.getContextPath()%>/admin/deleteproducts?id=${list.id}"><font color="red">Delete</font></a></td>
          
            
       </c:forEach>
        </tr>
        </table>
        
        
    </body>
</html>
 