<%-- 
    Document   : addproducts
    Created on : Jul 3, 2015, 5:15:41 PM
    Author     : subhashrijal
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
            <link rel="stylesheet" href="admincss.css" type="text/css" >
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
                <jsp:include page="dashboard.jsp"></jsp:include>
       
                 
            <form action="<%=request.getContextPath()%>/admin/addproducts" method="post" enctype="multipart/form-data">
           
                   
                    <table id="table1">
                      
                       
               <tr>
              
                   <td>Brand:</td>
                   <td><input type="text" name="brand"></td></tr>
               <font color ="red"><br/>${error} </font>
               <tr>
                   <td>Model:</td>
                   <td><input type="text" name="model"></td></tr>
               
               <tr>
                   <td>Cost</td>
                   <td><input type="text" name="cost"></td></tr>
               
               <tr>
                   <td>Stocks</td>
                   <td><input type="text" name="stocks"></td></tr>
               <tr>
                   <td>Image</td>
                   <td> <input type="file" name="picture"></td></tr>
           
       
               <tr><td><input type="submit" value="Add-product"></td></tr>
           </table>
        </form>
                
    </body>
    
</html>
