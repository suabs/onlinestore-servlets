<%-- 
    Document   : products
    Created on : Jul 3, 2015, 4:59:04 PM
    Author     : subhashrijal
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
            
            <link rel="stylesheet" href="admincss.css" type="text/css" >
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="dashboard.jsp"></jsp:include>
   
        <table>
    <th>id </th>
    <th>Brand-Name</th>
    <th>edit</th>
    <th>Delete</th>
    
    

        <c:forEach var="list" items="${products}">
            
             <tr><td>${list.id}</td>
             
             <td>${list.brand}</td>
             
             <td><a href="<%=request.getContextPath()%>/admin/editbrands?id=${list.id}"><font color="red">Edit</font></a></td>
             <td><a href="<%=request.getContextPath()%>/admin/deletebrands?id=${list.id}"><font color="red">delete</font></a></td>
            
       </c:forEach>
             
             
        </tr>
        </table>
        
    </body>
</html>
