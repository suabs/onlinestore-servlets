<%-- 
    Document   : userinfo
    Created on : Jul 19, 2015, 6:57:54 PM
    Author     : subhashrijal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="stylesheet.css" type="text/css" >
         <link rel="stylesheet" href="stylesheet1.css" type="text/css" >
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="header.jsp"></jsp:include>
        <jsp:include page="navigation.jsp"></jsp:include>
        <jsp:include page="loggerboard.jsp"></jsp:include>
        <jsp:include page="brandnavigation.jsp"></jsp:include>
        
        <div id="userprofile">
        <table>
           
             <div id="userprofiles">Information Check</div>
             
             
           
             <form action="<%= request.getContextPath()%>/loggeduser/updateinfo" method="post" >

            
            <tr>
               
                 <input type="hidden" name="id" value="${list.id}">
                <td><h4>First Name</h4></td>
               
                <td>  <input type="text" name="firstname" placeholder="Firstname" value="${list.firstname}"> </td>
          </tr>
          <tr>
          <font color="red"> ${error}</font>
              <td><h4>Last Name</h4></td>
              <td> <input type="text" name="lastname" placeholder="Lastname" value="${list.lastname}"></td>
          </tr>
          
           <tr>
              <td><h4>Username</h4></td>
              <td><font color="red">${list.username}</font></td>
          </tr>
          
           
          <tr>
              <td><h4>Address</h4></td>
              <td><input type="text" name="address" placeholder="Address" value="${list.address}"></td>
          </tr>
          
           <tr>
              <td><h4>Post Box</h4></td>
              <td><input type="text" name="areacode" placeholder="Post Box" value="${list.areacode}"></td>
          </tr>
          
           <tr>
              <td><h4>City</h4></td>
              <td><input type="text" name="city" placeholder="City" value="${list.city}"></td>
          </tr>
          <tr>
              <tr>
              <td><h4>Country</h4></td>
              <td><input type="text" name="country" placeholder="Country" value="${list.country}"></td>
          </tr>
               <tr>
              <td><h4>Phone Number</h4></td>
              <td><input type="text" name="phonenumber" placeholder="phone Number"value="${list.phone}"></td>
          </tr>
           <tr>
              <td><h4>Email</h4></td>
              <td><input type="text" name="email" placeholder="Email" value="${list.email}"></td>
          </tr>
          <tr> 
          
          </tr>
         
          <tr><td><input type="submit" value="Submit"></td></tr>
           
        </form>
           
        
        </table>
            </div>
            
    </body>
</html>
<jsp:include page="footer.jsp"></jsp:include>