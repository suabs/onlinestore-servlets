<%-- 
    Document   : brandnavigation
    Created on : Jul 7, 2015, 4:12:45 PM
    Author     : subhashrijal
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div id="brandnavigation">
                <div id="brands">Brands</div>
                
                <c:forEach var="list" items="${brands}">
                    <div id= "productbrands">
                        <a href="<%=request.getContextPath()%>/viewbrands?id=${list.brand}"> ${list.brand}</a>
                    
                    </div>
                </c:forEach>
                
            </div>
    </body>
</html>
