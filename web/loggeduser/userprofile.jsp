<%-- 
    Document   : userprofile
    Created on : Jul 9, 2015, 11:07:01 PM
    Author     : subhashrijal
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="stylesheet.css" type="text/css" >
        <link rel="stylesheet" href="stylesheet1.css" type="text/css" >
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
<jsp:include page="header.jsp"></jsp:include>
    <jsp:include page="navigation.jsp"></jsp:include>
     <jsp:include page="loggerboard.jsp"></jsp:include>       
     <jsp:include page="brandnavigation.jsp"></jsp:include>   



        <div id="userprofile">
        <table>
           
             <div id="userprofiles">User Profile</div>
           
           
            <c:forEach var="list" items="${userinfo}" >
            <tr>
                <td><h4>Firstname:</h4></td>
                <td> <font color="red"> ${list.firstname} </font></td>
          </tr>
          <tr>
              <td><h4>Lastname:</h4></td>
              <td> <font color="red">${list.lastname}</font> </td>
          </tr>
          <tr>
              <td><h4>Username:</h4></td>
              <td><font color="red"> ${list.username} </font></td>
          </tr>
          
          <tr>
              <td><h4>Address</h4></td>
              <td><font color ="red"> ${list.address}</font> </td>
          </tr>
          <tr>
              <td><h4>Post</h4></td>
              <td><font color="red"> ${list.areacode} </font></td>
          </tr>
          <tr>
              <td><h4>Country</h4></td>
              <td> <font color="red">${list.country} </font></td>
          </tr>
          <tr>
              <td><h4>Email</h4></td>
              <td><font color="red">  ${list.email}</font></td>
          
              
          </tr>
          <tr>
              <td><h4>User id</h4></td>
              <td> <font color="red">${list.id} </font></td>
          </tr>
         
          </c:forEach>
           
            
            
        
        </table>
            </div>
    </body>
</html>
<jsp:include page="footer.jsp"></jsp:include>