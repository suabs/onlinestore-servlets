<%-- 
    Document   : brandnavigation
    Created on : Jul 7, 2015, 4:12:45 PM
    Author     : subhashrijal
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        
        
        
        
        
        
        <div id="brandnavigation">
            
            
                <div id="brands">Brands</div>
              
                
                <c:forEach var ="list" items="${brands}">
                   <div id= "productbrands">
                       <a href="<%=request.getContextPath()%>/viewbrands?id=${list.brand}"> ${list.brand}</a>
                   </div>
                </c:forEach>
               
                
                <div id="loginform">
                    <div id="brands">Member Login</div>
                    
                    <table id="logintable">
                        <form action= "<%=request.getContextPath()%>/login " method="post">
                        <tr>
                        <font color ="white">${error1}</font>
                            <td><input type="text" name="username" placeholder="Username"></td>
                        
                        </tr>
                        <tr>
                            
                            <td><input type="password" name="password" placeholder="Password"></td>
                        </tr>
                        <td><input type="submit" value="Submit"></td>
                        <tr><td> <a href="<%=request.getContextPath()%>/forgotpassword">forgot password</a></td></tr>
                        <tr><td><a href="<%=request.getContextPath()%>/signup">signup</a></td></tr>
                        </form>
                        
                    </table>
                </div>
                
            </div>
    </body>
</html>
