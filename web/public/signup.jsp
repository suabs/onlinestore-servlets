<%-- 
    Document   : signup
    Created on : Jul 8, 2015, 12:02:24 AM
    Author     : subhashrijal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="public/stylesheet.css" type="text/css" >
        <link rel="stylesheet" href="public/stylesheet1.css" type="text/css" >
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="header.jsp"></jsp:include>
       <jsp:include page="navigation.jsp"></jsp:include>
       <jsp:include page="brandnavigation.jsp"></jsp:include>
       
       
       <div id="signup">
           <table>
               ${signup}
           <div id="signups">Signup</div>
        <form action="<%=request.getContextPath()%>/signup" method="post" >
            <tr>
                <td><h4>Firstname:</h4></td>
                <td style=""width:100px;height: 50px;"><input type="text" name="firstname" placeholder="Firstname"></td>
          </tr>
          <tr>
              <td><h4>Lastname:</h4></td>
              <td style="width:100px;height:50px;"><input type="text" name="lastname" placeholder="Lastname" ></td>
          </tr>
          <tr>
              <td><h4>Username:</h4></td>
               <td style="width:100px;height:50px;"><input type="text" name="username" placeholder="Username"></td>
          </tr>
          <tr>
              <td><h4>Password:</h4></td>
              <td><input type="password" name="password" placeholder="Password"></td>
          <font color ="red">${password}</font>
              
          </tr>
          <tr>
              <td><h4>Re-Password:</h4></td>
               <td style="width:100px;height:50px;"><input type="password" name="repassword" placeholder="Re-Password"></td>
          </tr>
         
          <tr>
              <td><h4>Email:</h4></td>
            <td style="width:100px;height:50px;"><input type="text" name="email" placeholder="Email"></td>
          </tr>
          
          <tr>
              <td><h4>Address</h4></td>
            <td style="width:100px;height:50px;"><input type="text" name="address" placeholder="Address"></td>
          </tr>
          <tr>
              <td><h4>Phone</h4></td>
            <td style="width:100px;height:50px;"><input type="text" name="phone" placeholder="Phone Number"></td>
          </tr>
          
          <tr>
              <td><h4>City</h4></td>
            <td style="width:100px;height:50px;"><input type="text" name="city" placeholder="City"></td>
          </tr>
          <tr>
              <td><h4>Area Code</h4></td>
            <td style="width:100px;height:50px;"><input type="text" name="areacode" placeholder="Area Code"></td>
          </tr>
          <tr>
              <td><h4>Country</h4></td>
            <td style="width:100px;height:50px;"><input type="text" name="country" placeholder="Country"></td>
          </tr>
          <tr>
             
          
          <td><input type="submit"  name="submit" value="Sign-up" id="submit"></td>
        </form>
        </table>
       </div>
            
    </body>
    <jsp:include page="footer.jsp"></jsp:include>
</html>
